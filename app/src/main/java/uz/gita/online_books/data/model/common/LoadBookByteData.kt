package uz.gita.online_books.data.model.common

data class LoadBookByteData(
    val book: BookResponseData,
    val byte: ByteArray
)
