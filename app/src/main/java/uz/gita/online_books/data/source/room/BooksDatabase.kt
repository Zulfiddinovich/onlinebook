package uz.gita.online_books.data.source.room

import androidx.room.Database
import androidx.room.RoomDatabase
import uz.gita.online_books.data.source.room.dao.BookDao
import uz.gita.online_books.data.source.room.entity.BookEntity

@Database(entities = [BookEntity::class], version = 2, exportSchema = false)
abstract class BooksDatabase: RoomDatabase() {
    abstract fun dao(): BookDao
}
