package uz.gita.online_books.data.source.room.dao

import androidx.room.*
import uz.gita.online_books.data.source.room.entity.BookEntity

@Dao
interface BookDao {
    @Query("SELECT * from books")
    fun getAllBooks(): List<BookEntity>

    @Query("SELECT * from books where fav = 1")
    fun getAllFavBooks(): List<BookEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertBooks(books: List<BookEntity>)

    @Query("DELETE from books where id > 0")
    fun deleteBooks()

    @Query("SELECT * from books where id = :id")
    fun getBook(id: Int): BookEntity

    @Update
    fun updateBook(book: BookEntity)

    @Query("UPDATE BOOKS SET lastPage = :lastPage where id = :bookId")
    fun updateBook(lastPage: Int, bookId: Int)
}