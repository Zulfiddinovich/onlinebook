package uz.gita.online_books.data.model.common

import uz.gita.online_books.data.model.response.AdminResponse

/*
   Author: Zukhriddin Kamolov
   Created: 02.08.2022 at 4:32
   Project: BookApp
*/

data class AdminData(
    val adminName: String,
    val password: String
){
    fun toAdminResponse() = AdminResponse(adminName, password)
}