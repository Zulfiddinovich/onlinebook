package uz.gita.online_books.data.model.response

import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.data.source.room.entity.BookEntity
import java.io.Serializable

data class BookResponse(
    val id: Int = 2,
    val image: String = "",
    val title: String = "",
    val description: String = "",
    val category: String = "",
    val author: String = "",
    val type: String = "",
    val size: String = "",
    val pages: Int = 0,
    val fav: Int = 0,
    val loadCount: Int = 0,
    val fileName: String = "",
    val url: String = "",
    val path: String = ""
): Serializable {

    fun toBookData(): BookResponseData {
        val _fav: Boolean = when (this.fav){
            0 ->{ false }
            else->{ true }
        }
        return BookResponseData(id, image, title, description, category, author, type, size, pages, fav = _fav, loadCount = loadCount, fileName = fileName, url = url, path = path)
    }

    fun toBookEntity(): BookEntity {
        val _fav: Boolean = when (this.fav){
            0 ->{ false }
            else->{ true }
        }
        return BookEntity(id, image, title, description, category, author, type, size, pages, fav = _fav, loading =  false, loadCount = loadCount, fileName = fileName, url = url, path = path, lastPage = 0)
    }
}
