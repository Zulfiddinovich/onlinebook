package uz.gita.online_books.data.model.response

import uz.gita.online_books.data.model.common.AdminData

/*
   Author: Zukhriddin Kamolov
   Created: 02.08.2022 at 4:32
   Project: BookApp
*/

data class AdminResponse(
    val adminName: String = "",
    val password: String = ""
){
    fun toAdminData() = AdminData(adminName, password)
}