package uz.gita.online_books.domain.usecase.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import uz.gita.online_books.data.model.common.AdminData
import uz.gita.online_books.domain.repository.BookRepository
import uz.gita.online_books.domain.usecase.AdminUseCase
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 02.08.2022 at 4:35
   Project: BookApp
*/

class AdminUseCaseImpl @Inject constructor(private val repository: BookRepository): AdminUseCase {

    override fun getAdminCredential() = callbackFlow<AdminData> {
        repository.getAdminCredential().collect {
            trySendBlocking(it.toAdminData())
        }

        awaitClose {  }
    }.flowOn(Dispatchers.IO)


}