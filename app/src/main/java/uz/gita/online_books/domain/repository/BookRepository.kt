package uz.gita.online_books.domain.repository

import android.net.Uri
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import uz.gita.online_books.data.model.common.AdminData
import uz.gita.online_books.data.model.common.BookAddRequestData
import uz.gita.online_books.data.model.request.BookAddRequest
import uz.gita.online_books.data.model.response.AdminResponse
import uz.gita.online_books.data.model.response.BookResponse
import uz.gita.online_books.data.source.room.entity.BookEntity


interface BookRepository {

    fun getBooksList(): Flow<List<BookResponse>>

    fun getBooksListDB(): Flow<List<BookResponse>>

//    fun getFavouriteBooksList(): Flow<List<BookResponse>>

    fun getFavouriteBooksListDB(): Flow<List<BookResponse>>

    fun getBookListSize(): Int

    fun uploadBook(book: BookAddRequest, bookFileUri: Uri): Flow<Boolean>

    fun uploadBookImage(book: BookAddRequest, imageUri: Uri): Flow<Boolean>

    fun loadBook(book: BookResponse): Flow<Boolean>

//    fun isBookFavourite(book: BookAddRequest): Flow<Boolean>

    fun isBookFavouriteDB(book: BookAddRequest): Flow<Boolean>

    fun addBookLoadCounter(book: BookAddRequest): Flow<Boolean>

    fun getLastPage(id: Int): Int

    fun setLastPage(book: BookEntity)

    fun getAdminCredential(): Flow<AdminResponse>

}