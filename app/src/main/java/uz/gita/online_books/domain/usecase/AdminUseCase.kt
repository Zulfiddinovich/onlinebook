package uz.gita.online_books.domain.usecase

import kotlinx.coroutines.flow.Flow
import uz.gita.online_books.data.model.common.AdminData

/*
   Author: Zukhriddin Kamolov
   Created: 02.08.2022 at 4:35
   Project: BookApp
*/

interface AdminUseCase {

    fun getAdminCredential(): Flow<AdminData>
}