package uz.gita.online_books.domain.usecase

import android.net.Uri
import kotlinx.coroutines.flow.Flow
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.data.model.request.BookAddRequest


interface BookUseCase {

    fun getBooksList(): Flow<List<BookResponseData>>

    fun getBooksListDB(): Flow<List<BookResponseData>>

    fun getFavouriteBooksListDB(): Flow<List<BookResponseData>>

//    fun uploadBook(book: BookAddRequest, imageUri: Uri): Flow<Boolean>

    fun loadBook(book: BookResponseData): Flow<Boolean>

    fun isBookFavourite(book: BookAddRequest): Flow<Boolean>

    fun addBookLoadCounter(book: BookAddRequest): Flow<Boolean>
}