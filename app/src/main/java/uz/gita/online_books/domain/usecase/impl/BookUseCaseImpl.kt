package uz.gita.online_books.domain.usecase.impl

import android.net.Uri
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import timber.log.Timber
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.data.model.request.BookAddRequest
import uz.gita.online_books.domain.repository.BookRepository
import uz.gita.online_books.domain.usecase.BookUseCase
import javax.inject.Inject


class BookUseCaseImpl @Inject constructor(private val repository: BookRepository): BookUseCase {
    private val TAG = "BookUseCaseImpl"

    override fun getBooksList(): Flow<List<BookResponseData>> = flow {
        repository.getBooksList().collect {
            Timber.tag(TAG).d( " usecase getBooksList: list get success, ")
            val temp = it.map { it.toBookData() }
            emit(temp)
        }
    }.flowOn(Dispatchers.IO)

    override fun getBooksListDB(): Flow<List<BookResponseData>> = flow {
        repository.getBooksListDB().collect {
            Timber.tag(TAG).d( " usecase getBooksList: list DB, ")
            val temp = it.map { it.toBookData() }
            emit(temp)
        }
    }.flowOn(Dispatchers.IO)

    override fun getFavouriteBooksListDB(): Flow<List<BookResponseData>> = flow {
        repository.getFavouriteBooksListDB().collect {
            Timber.tag(TAG).d( " usecase getBooksList: list DB, ")
            val temp = it.map { it.toBookData() }
            emit(temp)
        }
    }.flowOn(Dispatchers.IO)

//    override fun uploadBook(book: BookAddRequest, imageUri: Uri): Flow<Boolean> = repository.uploadBook(book, imageUri)

    override fun loadBook(book: BookResponseData): Flow<Boolean> = flow {
         repository.loadBook(book.toBookResponse()).collect {
             Timber.tag(TAG).d( " usecase loadBook: success, " + it)
             emit(it)
         }
    }.flowOn(Dispatchers.IO)

    override fun isBookFavourite(book: BookAddRequest): Flow<Boolean> = flow {
        repository.isBookFavouriteDB(book).collect {
            emit(it)
        }
    }.flowOn(Dispatchers.IO)

    override fun addBookLoadCounter(book: BookAddRequest): Flow<Boolean> = flow {
        repository.addBookLoadCounter(book).collect{
            emit(it)
        }
    }.flowOn(Dispatchers.IO)


}