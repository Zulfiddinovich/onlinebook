package uz.gita.online_books.domain.usecase

import kotlinx.coroutines.flow.Flow
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.data.model.request.BookAddRequest


interface FavUseCase {

//    fun getFavouriteBooksList(): Flow<List<BookResponseData>>

    fun getFavouriteBooksListDB(): Flow<List<BookResponseData>>

    fun loadBook(book: BookResponseData): Flow<Boolean>

    fun isBookFavourite(book: BookAddRequest): Flow<Boolean>

    fun addBookLoadCounter(book: BookAddRequest): Flow<Boolean>
}