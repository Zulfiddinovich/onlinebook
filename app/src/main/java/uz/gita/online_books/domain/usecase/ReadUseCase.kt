package uz.gita.online_books.domain.usecase

import uz.gita.online_books.data.model.common.BookResponseData

/*
   Author: Zukhriddin Kamolov
   Created: 01.08.2022 at 3:03
   Project: BookApp
*/

interface ReadUseCase {

    fun getLastPage(id: Int): Int

    fun setLastPage(book: BookResponseData)

}