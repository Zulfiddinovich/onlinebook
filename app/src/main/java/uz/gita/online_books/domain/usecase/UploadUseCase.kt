package uz.gita.online_books.domain.usecase

import android.net.Uri
import kotlinx.coroutines.flow.Flow
import uz.gita.online_books.data.model.common.BookAddRequestData
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.data.model.request.BookAddRequest

/*
   Author: Zukhriddin Kamolov
   Created: 01.08.2022 at 3:03
   Project: BookApp
*/

interface UploadUseCase {

    fun getBookListSize(): Int

    fun uploadBook(book: BookAddRequestData, bookFileUri: Uri): Flow<Boolean>

    fun uploadBookImage(book: BookAddRequestData, imageUri: Uri): Flow<Boolean>

}