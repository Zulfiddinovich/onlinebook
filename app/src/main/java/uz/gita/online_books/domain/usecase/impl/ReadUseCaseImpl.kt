package uz.gita.online_books.domain.usecase.impl

import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.domain.repository.BookRepository
import uz.gita.online_books.domain.usecase.ReadUseCase
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 01.08.2022 at 3:03
   Project: BookApp
*/

class ReadUseCaseImpl @Inject constructor(private val repository: BookRepository): ReadUseCase {
    override fun getLastPage(id: Int): Int = repository.getLastPage(id)

    override fun setLastPage(book: BookResponseData) = repository.setLastPage(book.toBookEntity())


}