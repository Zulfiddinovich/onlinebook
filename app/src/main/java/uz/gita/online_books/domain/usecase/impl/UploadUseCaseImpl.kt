package uz.gita.online_books.domain.usecase.impl

import android.net.Uri
import kotlinx.coroutines.flow.Flow
import uz.gita.online_books.data.model.common.BookAddRequestData
import uz.gita.online_books.domain.repository.BookRepository
import uz.gita.online_books.domain.usecase.UploadUseCase
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 01.08.2022 at 3:03
   Project: BookApp
*/

class UploadUseCaseImpl @Inject constructor(private val repository: BookRepository): UploadUseCase {

    override fun getBookListSize(): Int = repository.getBookListSize()

    override fun uploadBook(book: BookAddRequestData, bookFileUri: Uri): Flow<Boolean> = repository.uploadBook(book.toBookAddRequest(), bookFileUri)

    override fun uploadBookImage(book: BookAddRequestData, imageUri: Uri): Flow<Boolean> = repository.uploadBookImage(book.toBookAddRequest(), imageUri)
}