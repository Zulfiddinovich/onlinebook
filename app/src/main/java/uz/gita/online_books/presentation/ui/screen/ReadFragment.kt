package uz.gita.online_books.presentation.ui.screen

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import uz.gita.online_books.R
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.databinding.FragmentReadBinding
import uz.gita.online_books.presentation.viewmodel.ReadViewModel
import uz.gita.online_books.presentation.viewmodel.impl.ReadViewModelImpl
import java.io.File

@AndroidEntryPoint
class ReadFragment : Fragment(R.layout.fragment_read) {
    private val binding by viewBinding(FragmentReadBinding::bind)
    private val viewModel: ReadViewModel by viewModels<ReadViewModelImpl>()
    private val TAG = "ReadFragment"

    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val data = arguments?.getSerializable("read_book") as BookResponseData
        binding.titleReading.text = data.title



        val path1 = Uri.fromFile(File(data.path.trim()))
//        MediaScannerConnection.scanFile(requireContext(), arrayOf(File(data.path.trim()).getAbsolutePath()), null,
//            OnScanCompletedListener { path2: String?, uri2: Uri ->
//                Timber.tag(TAG).d( "Path1: " + path1 + ", Path2: " + path2 + ", Uri2: " + uri2.toString())
//                loadFromUri(path1)
//            })
        loadFromUri(path1, data)

        binding.backButton.setOnClickListener {
            viewModel.goBack()
        }

        viewModel.goBackLiveData.observe(this, goBackObserver)
    }

    private val goBackObserver = Observer<Unit>{
        findNavController().popBackStack()
    }

    private fun loadFromUri(uri: Uri, book: BookResponseData) {

        Timber.tag(TAG).d( "load TRY")
        binding.pdfViewer.fromUri(uri)
            .defaultPage(viewModel.getLastPage(book.id))
            .swipeHorizontal(false)
            .onPageChange{ page, pageCount -> // current and totalPage

                Timber.tag(TAG).d( "loadFromUri: page-${page},  pageCount-${pageCount}")
                val currentPage = page + 1
                viewModel.setLastPage(book.copy(lastPage = page))
                binding.pageCounter.text = "page ${currentPage} / ${pageCount}"
            }
            .onError { e->
                Timber.tag(TAG).d("error: " + e.message)
            }
            .onPageError{ page, e ->
                Timber.tag(TAG).d("errorPage: " + e.message)
            }.load()
    }

}
