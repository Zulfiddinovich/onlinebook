package uz.gita.online_books.presentation.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import kotlinx.coroutines.flow.Flow
import uz.gita.online_books.data.model.common.BookAddRequestData

/*
   Author: Zukhriddin Kamolov
   Created: 01.08.2022 at 16:52
   Project: BookApp
*/

interface UploadViewModel {

    val goBackLiveData: LiveData<Unit>
    val uploadSuccessLiveData: LiveData<Unit>
    val uploadErrorLiveData: LiveData<Unit>
    val progressLiveData: LiveData<Boolean>
    val messageLiveData: LiveData<String>

    fun getBookListSize(): Int

    fun uploadBook(book: BookAddRequestData, imageUri: Uri,  bookFileUri: Uri)

    fun uploadBookImage(book: BookAddRequestData, imageUri: Uri)

    fun goBack()
}