package uz.gita.online_books.presentation.viewmodel.impl

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.gita.mynewsapp.utils.isConnected
import uz.gita.online_books.data.model.common.AdminData
import uz.gita.online_books.domain.usecase.AdminUseCase
import uz.gita.online_books.presentation.viewmodel.AdminViewModel
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 02.08.2022 at 4:23
   Project: BookApp
*/
@HiltViewModel
class AdminViewModelImpl @Inject constructor(private val useCase: AdminUseCase): ViewModel(), AdminViewModel {
    override val goBackLiveData = MutableLiveData<Unit>()
    override val goUploadScreenLiveData = MutableLiveData<Unit>()
    override val adminPasswordCorrect = MutableLiveData<Unit>()
    override val adminPasswordIncorrect = MutableLiveData<Unit>()
    override val progressLiveData = MutableLiveData<Boolean>()
    override val messageLiveData = MutableLiveData<String>()


    override fun checkAdminCredential(admin: AdminData) {
        if (isConnected()) {
            progressLiveData.value = true
            useCase.getAdminCredential().onEach {
                if (admin.adminName == it.adminName && admin.password == it.password) {
                    adminPasswordCorrect.value = Unit
                } else {
                    adminPasswordIncorrect.value = Unit
                }
                progressLiveData.value = false
            }.launchIn(viewModelScope)
        } else {
            messageLiveData.value = "Internet connection lost"
        }
    }

    override fun goUploadScreen() {
        goUploadScreenLiveData.value = Unit
    }

    override fun goBack() {
        goBackLiveData.value = Unit
    }


}