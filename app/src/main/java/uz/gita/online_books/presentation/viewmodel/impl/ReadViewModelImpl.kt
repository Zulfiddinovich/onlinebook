package uz.gita.online_books.presentation.viewmodel.impl

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.domain.usecase.ReadUseCase
import uz.gita.online_books.presentation.viewmodel.ReadViewModel
import javax.inject.Inject

@HiltViewModel
class ReadViewModelImpl @Inject constructor(private val useCase: ReadUseCase): ViewModel(), ReadViewModel {
    override val goBackLiveData = MutableLiveData<Unit>()

    override fun goBack() {
        goBackLiveData.value = Unit
    }

    override fun getLastPage(id: Int): Int = useCase.getLastPage(id)

    override fun setLastPage(book: BookResponseData) = useCase.setLastPage(book)

}