package uz.gita.online_books.presentation.viewmodel.impl

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import uz.gita.mynewsapp.utils.isConnected
import uz.gita.online_books.data.model.common.BookAddRequestData
import uz.gita.online_books.domain.usecase.UploadUseCase
import uz.gita.online_books.presentation.viewmodel.UploadViewModel
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 01.08.2022 at 16:52
   Project: BookApp
*/

@HiltViewModel
class UploadViewModelImpl @Inject constructor(private val useCase: UploadUseCase): ViewModel(), UploadViewModel {
    private val TAG = "UploadViewModelImpl"
    override val goBackLiveData = MutableLiveData<Unit>()
    override val uploadSuccessLiveData = MutableLiveData<Unit>()
    override val uploadErrorLiveData = MutableLiveData<Unit>()
    override val progressLiveData = MutableLiveData<Boolean>()
    override val messageLiveData = MutableLiveData<String>()


    override fun getBookListSize(): Int = useCase.getBookListSize()

    override fun uploadBook(book: BookAddRequestData, imageUri: Uri, bookFileUri: Uri) {
        if (isConnected()){
        progressLiveData.value = true
            useCase.uploadBook(book, bookFileUri).onEach {
                if (it) uploadBookImage(book, imageUri)
                else {
                    Timber.tag(TAG).d("something went wrong")
                    uploadErrorLiveData.value = Unit
                    progressLiveData.value = false
                }
            }.launchIn(viewModelScope)
        } else {
            messageLiveData.value = "Internet connection lost"
        }
    }

    override fun uploadBookImage(book: BookAddRequestData, imageUri: Uri) {
        useCase.uploadBookImage(book, imageUri).onEach {
            if (it) {
                Timber.tag(TAG).d("image loaded")
                uploadSuccessLiveData.value = Unit
                progressLiveData.value = false
            }
            else {
                Timber.tag(TAG).d("something went wrong")
                uploadErrorLiveData.value = Unit
                progressLiveData.value = false
            }
        }.launchIn(viewModelScope)
    }

    override fun goBack() {
        goBackLiveData.value = Unit
    }
}