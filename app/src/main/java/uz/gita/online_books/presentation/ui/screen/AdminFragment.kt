package uz.gita.online_books.presentation.ui.screen

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.gita.online_books.R
import uz.gita.online_books.data.model.common.AdminData
import uz.gita.online_books.databinding.FragmentAdminBinding
import uz.gita.online_books.presentation.viewmodel.AdminViewModel
import uz.gita.online_books.presentation.viewmodel.impl.AdminViewModelImpl

@AndroidEntryPoint
class AdminFragment : Fragment(R.layout.fragment_admin) {
    private val binding by viewBinding(FragmentAdminBinding::bind)
    private val viewModel: AdminViewModel by viewModels<AdminViewModelImpl>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        liveDatas()  // onCreateda 1 marta chaqirilyabdi - liveDatalar qayta qayta observe qilmasligi uchun
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.progressBar.isVisible = false
        clicks()
    }

    private fun liveDatas() {
        viewModel.adminPasswordCorrect.observe(this){
            Log.d("TAG", "liveDatas: adminPasswordCorrect")
            viewModel.goUploadScreen()
        }
        viewModel.adminPasswordIncorrect.observe(this){
            Toast.makeText(requireContext(), "Login or password is incorrect!", Toast.LENGTH_SHORT).show()
        }
        viewModel.goBackLiveData.observe(this){
            findNavController().popBackStack()
        }
        viewModel.goUploadScreenLiveData.observe(this){
            Log.d("TAG", "goUploadScreenLiveData")
            findNavController().navigate(AdminFragmentDirections.actionAdminFragmentToUploadBookFragment())
        }
        viewModel.messageLiveData.observe(this){
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
        viewModel.progressLiveData.observe(this){
            binding.progressBar.isVisible = it
        }
    }

    private fun clicks() {

        binding.enterButton.setOnClickListener {
            val login = binding.adminLoginEt.text.toString().trim()
            val password = binding.adminPassEt.text.toString().trim()
            if (login.isNotEmpty()) {
                if (password.isNotEmpty()) {
                    //
                    viewModel.checkAdminCredential(AdminData(login,password))
                    //
                } else Toast.makeText(requireContext(), "Fill the password field", Toast.LENGTH_SHORT).show()
            } else  Toast.makeText(requireContext(), "Fill the login field", Toast.LENGTH_SHORT).show()
        }

        binding.backButton.setOnClickListener {
            viewModel.goBack()
        }

    }


}