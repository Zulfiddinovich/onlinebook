package uz.gita.online_books.presentation.ui.screen

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import uz.gita.online_books.R
import uz.gita.online_books.data.model.common.BookAddRequestData
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.databinding.FragmentFavouriteBinding
import uz.gita.online_books.databinding.LoadingDilaogBinding
import uz.gita.online_books.presentation.ui.adapter.FavListAdapter
import uz.gita.online_books.presentation.viewmodel.impl.FavViewModelImpl
import uz.gita.online_books.utils.checkPermissions
import uz.gita.online_books.utils.loadCompleteLiveData
import uz.gita.online_books.utils.loadStartedLiveData

@AndroidEntryPoint
class FavouriteFragment : Fragment(R.layout.fragment_favourite) {
    private val binding by viewBinding(FragmentFavouriteBinding::bind)
    private val viewModel by viewModels<FavViewModelImpl>()
    private lateinit var mFavAdapter: FavListAdapter
    private var loadingDialog: AlertDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setAdapter()
        livedata()
        clicks()

    }

    override fun onResume() {
        super.onResume()
        Log.d("TAG", "FavouriteFragment onResume: called")
        viewModel.getFavouriteBooksListDB()
    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun livedata() {
        viewModel.bookListResponseData.observe(viewLifecycleOwner, bookListObserver)
        viewModel.readBookLiveData.observe(this, readBookObserver)
        viewModel.addBookLoadCounterLiveData.observe(this, addBookLoadCounterObserver)

        viewModel.loadSuccessLiveData.observe(this, loadSuccessObserver)

        loadStartedLiveData.observe(this, loadStartedObserver)
        loadCompleteLiveData.observe(this, loadHelperObserver)
    }

    private val bookListObserver = Observer<List<BookResponseData>> {
        if (it.size > 0) {
            binding.favRecycle.isVisible = true
            binding.animationView.isVisible = false
            mFavAdapter.submitList(it)
        } else {
            binding.favRecycle.isVisible = false
            binding.animationView.isVisible = true
        }
    }

    private val loadSuccessObserver = Observer<Boolean> {
        // mainFragmentda toat qilingan shuning uchun bu bo`sh funksiya
    }

    private val readBookObserver = Observer<BookResponseData> {
        val readBundle = bundleOf("read_book" to it)
        findNavController().navigate(R.id.readFragment, readBundle)
    }

    private val addBookLoadCounterObserver = Observer<Boolean> {
        // mainFragmentda toast qilingan shuning uchun bu bo`sh funksiya
    }

    private val loadHelperObserver = Observer<BookAddRequestData> {
        // dialog dismiss
        if (loadingDialog != null && loadingDialog!!.isShowing) {
            Log.d("TAG", "addBookLoadCounter Favourite worked")
            viewModel.addBookLoadCounter(it)

            loadingDialog!!.dismiss()
        }
    }
    private val loadStartedObserver = Observer<BookAddRequestData> {
        // dialog ochmayabmiz, chunki mainFragmentda ochiladi, chunki liveData 1ta
        // openDialog(it)
    }

    private fun openDialog(book: BookAddRequestData){
        val binding = LoadingDilaogBinding.inflate(LayoutInflater.from(requireContext()), null, false)
        binding.title.text = book.title
        Glide.with(binding.image).load(R.drawable.loading).into(binding.image)

        loadingDialog = AlertDialog.Builder(requireContext())
            .setCancelable(false)
            .setView(binding.root)
            .create()

        loadingDialog!!.show()
    }


    private fun setAdapter() {
        mFavAdapter = FavListAdapter()
        binding.favRecycle.adapter = mFavAdapter
        binding.favRecycle.layoutManager = LinearLayoutManager(requireContext())
        binding.favRecycle.isVisible = false
    }

    private fun clicks() {
        mFavAdapter.putDownloadListener { book ->
            requireActivity().checkPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                Timber.tag("TAG").d("mainFragment WRITE_EXTERNAL_STORAGE permission granted")
                viewModel.loadBook(book)
            }
        }


        mFavAdapter.putReadListener {
            requireActivity().checkPermissions(arrayOf( android.Manifest.permission.READ_EXTERNAL_STORAGE)){
                Timber.tag("TAG").d("mainFragment READ_EXTERNAL_STORAGE permission granted")
                viewModel.readBook(it)
            }
        }

        mFavAdapter.isFavListener {
            viewModel.isBookFavourite(it)
        }
    }



}