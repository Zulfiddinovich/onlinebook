package uz.gita.online_books.presentation.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import uz.gita.online_books.data.model.common.BookAddRequestData
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.data.model.request.BookAddRequest


interface MainViewModel {
    val bookListResponseData: LiveData<List<BookResponseData>>
    val favBookListLiveResponseData: LiveData<List<BookResponseData>>
    val uploadSuccessLiveData: LiveData<Boolean>
    val loadSuccessLiveData: LiveData<Boolean>
    val readBookLiveData: LiveData<BookResponseData>

    val isBookFavouriteLiveData: LiveData<Boolean>
    val addBookLoadCounterLiveData: LiveData<Boolean>

    fun getBooksList()

    fun getBooksListDB()

//    fun uploadBook(book: BookAddRequest, imageUri: Uri)

    fun loadBook(book: BookResponseData)

    fun readBook(book: BookResponseData)

    fun isBookFavourite(book: BookAddRequestData)

    fun addBookLoadCounter(book: BookAddRequestData)

}