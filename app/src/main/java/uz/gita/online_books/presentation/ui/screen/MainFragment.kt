package uz.gita.online_books.presentation.ui.screen

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import uz.gita.online_books.R
import uz.gita.online_books.data.model.common.BookAddRequestData
import uz.gita.online_books.data.model.common.BookResponseData
import uz.gita.online_books.databinding.FragmentMainBinding
import uz.gita.online_books.databinding.LoadingDilaogBinding
import uz.gita.online_books.presentation.ui.adapter.BookListAdapter
import uz.gita.online_books.presentation.viewmodel.MainViewModel
import uz.gita.online_books.presentation.viewmodel.impl.MainViewModelImpl
import uz.gita.online_books.utils.checkPermissions
import uz.gita.online_books.utils.loadCompleteLiveData
import uz.gita.online_books.utils.loadStartedLiveData
import java.io.File


@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {
    private val binding by viewBinding(FragmentMainBinding::bind)
    private val viewModel: MainViewModel by viewModels<MainViewModelImpl>()
    private lateinit var booksAdapter: BookListAdapter
    private var loadingDialog: AlertDialog? = null
    private val TAG = "MainFragment"


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setAdapter()
        livedata()
        clicks()

    }


    private fun setAdapter() {
        binding.booksRecycle.isVisible = false
        booksAdapter = BookListAdapter()
        binding.booksRecycle.adapter = booksAdapter
        binding.booksRecycle.layoutManager = LinearLayoutManager(requireContext())
    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun livedata() {
        viewModel.bookListResponseData.observe(viewLifecycleOwner, bookListObserver)
        viewModel.uploadSuccessLiveData.observe(this, uploadSuccessObserver)
        viewModel.loadSuccessLiveData.observe(this, loadSuccessObserver)
        viewModel.readBookLiveData.observe(this, readBookObserver)
        viewModel.addBookLoadCounterLiveData.observe(this, addBookLoadCounterObserver)
        loadStartedLiveData.observe(this, loadStartedObserver)
        loadCompleteLiveData.observe(this, loadHelperObserver)
    }

    private val bookListObserver = Observer<List<BookResponseData>> {
        if (it.size > 0){
            binding.booksRecycle.isVisible = true
            binding.animationView.isVisible = false
            booksAdapter.submitList(it)
        } else {
            binding.booksRecycle.isVisible = false
            binding.animationView.isVisible = true
        }
    }
    private val uploadSuccessObserver = Observer<Boolean> {
        Toast.makeText(requireContext(), "Uploaded: " + it, Toast.LENGTH_SHORT).show()
    }

    private val loadSuccessObserver = Observer<Boolean> {
        // loadCompleteLiveData // bilan bir xil yangilanyabdi, shuning uchun bo`sh
    }
    private val readBookObserver = Observer<BookResponseData> {
        val readBundle = bundleOf("read_book" to it)
        findNavController().navigate(R.id.readFragment, readBundle)

    }
    private val addBookLoadCounterObserver = Observer<Boolean> {
        when(it){
            true -> Toast.makeText(requireContext(), "Book loaded", Toast.LENGTH_SHORT).show()
            else -> Toast.makeText(requireContext(), "Book load failed", Toast.LENGTH_SHORT).show()
        }
    }

    private val loadHelperObserver = Observer<BookAddRequestData> {
        // dialog dismiss
        Timber.tag(TAG).d( "loadHelperObserver: dialogDismiss")
        if (loadingDialog != null && loadingDialog!!.isShowing) {
            Log.d("TAG", "addBookLoadCounter Main worked")
            viewModel.addBookLoadCounter(it)

            loadingDialog!!.dismiss()
            booksAdapter.notifyDataSetChanged()
        }
    }

    private val loadStartedObserver = Observer<BookAddRequestData> {
        // dialog start
        openDialog(it)
    }

    private fun openDialog(book: BookAddRequestData){
        val binding = LoadingDilaogBinding.inflate(LayoutInflater.from(requireContext()), null, false)
        binding.title.text = book.title
        Glide.with(binding.image).load(R.drawable.loading).into(binding.image)

        loadingDialog = AlertDialog.Builder(requireContext())
            .setCancelable(false)
            .setView(binding.root)
            .create()

        loadingDialog!!.show()
    }

    private fun clicks() {

        booksAdapter.putDownloadListener { book ->
            requireActivity().checkPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                viewModel.loadBook(book)
            }
        }


        booksAdapter.putReadListener {
            val uri = Uri.fromFile(File(it.path.trim()))
            checkFromUri(uri, it)
        }

        booksAdapter.isFavListener {
            viewModel.isBookFavourite(it)
        }
    }

    private fun checkFromUri(uri: Uri, book: BookResponseData) {
        binding.pdfViewer.fromUri(uri)
            .swipeHorizontal(false)
            .onPageChange{ page, pageCount -> // current and totalPage
                Timber.tag(TAG).d( "loadFromUri: page-${page},  pageCount-${pageCount}")
                requireActivity().checkPermissions(arrayOf( android.Manifest.permission.READ_EXTERNAL_STORAGE)){
                    viewModel.readBook(book)
                }
            }
            .onError { e->
                Timber.d("error 2: " + e.message)
                if ( e.message.toString().startsWith("uz.gita.online_books has no access") ) {
                    requireActivity().checkPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                        viewModel.loadBook(book)
                    }
                }
            }
            .onPageError{ page, e ->
                Timber.d("errorPage: " + e.message)
            }.load()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBooksListDB()
    }

}