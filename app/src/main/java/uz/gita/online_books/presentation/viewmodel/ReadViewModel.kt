package uz.gita.online_books.presentation.viewmodel

import androidx.lifecycle.LiveData
import uz.gita.online_books.data.model.common.BookResponseData


interface ReadViewModel {
    val goBackLiveData: LiveData<Unit>

    fun goBack()

    fun getLastPage(id: Int): Int

    fun setLastPage(book: BookResponseData)

}