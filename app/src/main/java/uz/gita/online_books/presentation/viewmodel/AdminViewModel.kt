package uz.gita.online_books.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import uz.gita.online_books.data.model.common.AdminData

/*
   Author: Zukhriddin Kamolov
   Created: 02.08.2022 at 4:23
   Project: BookApp
*/

interface AdminViewModel {
    val goBackLiveData: LiveData<Unit>
    val goUploadScreenLiveData: LiveData<Unit>
    val adminPasswordCorrect: LiveData<Unit>
    val adminPasswordIncorrect: LiveData<Unit>
    val progressLiveData: LiveData<Boolean>
    val messageLiveData: LiveData<String>

    fun checkAdminCredential(admin: AdminData)

    fun goUploadScreen()

    fun goBack()

}