package uz.gita.online_books.presentation.ui.screen

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import uz.gita.online_books.R
import uz.gita.online_books.data.model.common.BookAddRequestData
import uz.gita.online_books.databinding.FragmentUploadBookBinding
import uz.gita.online_books.presentation.viewmodel.UploadViewModel
import uz.gita.online_books.presentation.viewmodel.impl.UploadViewModelImpl
import uz.gita.online_books.utils.checkPermissions
import java.io.File

@AndroidEntryPoint
class UploadBookFragment : Fragment(R.layout.fragment_upload_book) {
    private val binding by viewBinding(FragmentUploadBookBinding::bind)
    private val viewModel: UploadViewModel by viewModels<UploadViewModelImpl>()
    private val TAG = "UploadBookFragment"
    private val PICK_IMAGE = 1010
    private val PICK_BOOK = 2020
    private var pickedImageUri: Uri? = null
    private var pickedBookUri: Uri? = null
    private var bookFileName = ""
    private var selectedCategory: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        liveDatas()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setView()
        clicks()

    }

    private fun setView() {
        Glide.with(binding.bookImageView).load(R.drawable.add_photo1).into(binding.bookImageView)
        binding.progressBar.isVisible = false

        val adapter = ArrayAdapter.createFromResource(requireContext(), R.array.category_spinner_array, R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        binding.categorySpinner.adapter = adapter
        binding.categorySpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedCategory = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun liveDatas() {
        viewModel.uploadSuccessLiveData.observe(this){
            Timber.tag(TAG).d("upload success")
            Toast.makeText(requireContext(), "Book uploaded successfully", Toast.LENGTH_SHORT).show()
            lifecycleScope.launch {
                delay(400)
                findNavController().navigate(UploadBookFragmentDirections.actionUploadBookFragmentToHostFragment())
            }
        }
        viewModel.uploadErrorLiveData.observe(this){
            Timber.tag(TAG).d("upload not success")
        }
        viewModel.progressLiveData.observe(this){
            binding.progressBar.isVisible = it
        }
        viewModel.messageLiveData.observe(this){
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
        viewModel.goBackLiveData.observe(this){
            findNavController().popBackStack()
        }

    }

    private fun clicks() {
        binding.continueButton.setOnClickListener{
            if (pickedBookUri != null) {
                if (pickedImageUri != null) {
                    if (selectedCategory.isNotEmpty()) {
                        if (binding.bookNameEt.text.toString().trim().isNotEmpty()) {
                            if (binding.bookDescriptionEt.text.toString().trim().isNotEmpty()) {
                                if (binding.pagesEt.text.toString().trim().isNotEmpty()) {
                                    if (binding.sizeEt.text.toString().trim().isNotEmpty()) {
                                        if (binding.authorEt.text.toString().trim().isNotEmpty()) {
                                            val fileName = binding.bookNameEt.text.toString().replace(" ", "_").trim()
                                            Timber.tag(TAG).d("clicks: fileName: ${fileName}, category: ${selectedCategory}")

                                            val temp = BookAddRequestData(
                                                id = viewModel.getBookListSize() + 1,
                                                image = "",
                                                title = binding.bookNameEt.text.toString(),
                                                description = binding.bookDescriptionEt.text.toString(),
                                                category = selectedCategory,
                                                author = binding.authorEt.text.toString(),
                                                type = "pdf",
                                                size = binding.sizeEt.text.toString() + "mb",
                                                pages = binding.pagesEt.text.toString().toInt(),
                                                fav = false,
                                                loadCount = 0,
                                                fileName = fileName,
                                                url = "",
                                                path = "/data/user/0/uz.gita.online_books/files/mydir/" + fileName + ".pdf"
                                            )
                                            viewModel.uploadBook(temp, pickedImageUri!!, pickedBookUri!!)
                                        } else Toast.makeText(requireContext(), "Fill the author name field", Toast.LENGTH_SHORT).show()
                                    } else Toast.makeText(requireContext(), "Write book size", Toast.LENGTH_SHORT).show()
                                } else Toast.makeText(requireContext(), "Write pages", Toast.LENGTH_SHORT).show()
                            } else Toast.makeText(requireContext(), "Fill the description field", Toast.LENGTH_SHORT).show()
                        } else Toast.makeText(requireContext(), "Fill the name field", Toast.LENGTH_SHORT).show()
                    } else Toast.makeText(requireContext(), "You didn`t choose category", Toast.LENGTH_SHORT).show()
                } else Toast.makeText(requireContext(), "You didn`t pick an image", Toast.LENGTH_SHORT).show()
            } else Toast.makeText(requireContext(), "You didn`t pick a book file", Toast.LENGTH_SHORT).show()
        }

        binding.bookImageView.setOnClickListener {
            requireActivity().checkPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                val intent = Intent().setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        }

        binding.uploadButton.setOnClickListener {
            requireActivity().checkPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                val intent = Intent().setType("application/pdf")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(Intent.createChooser(intent, "Select Book file"), PICK_BOOK)
            }
        }

        binding.backButton.setOnClickListener {
            viewModel.goBack()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            pickedImageUri = data?.data
            Glide.with(binding.bookImageView).load(pickedImageUri).into(binding.bookImageView)
        }
        if (requestCode == PICK_BOOK && resultCode == Activity.RESULT_OK){
            pickedBookUri = data?.data
            pickedBookUri?.let {
                val bookFile = File(it.path.toString())
                bookFileName = bookFile.nameWithoutExtension
                binding.bookFileNameTv.text = "book: "+bookFileName+".pdf"
            }
        }
    }

}