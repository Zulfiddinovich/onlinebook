package uz.gita.online_books.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class BooksCollectionRef

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class AdminCollectionRef

@Module
@InstallIn(SingletonComponent::class)
class FirebaseModule {

    @[Provides Singleton]
    fun provideDatabase(): FirebaseFirestore = FirebaseFirestore.getInstance()

    @[Provides Singleton]
    fun provideAuth(): FirebaseAuth = Firebase.auth // FirebaseAuth.getInstance()

    @[Provides Singleton BooksCollectionRef]
    fun bookStoreReference(database: FirebaseFirestore): CollectionReference = database.collection("books")

    @[Provides Singleton AdminCollectionRef]
    fun adminStoreReference(database: FirebaseFirestore): CollectionReference = database.collection("admin")

    @[Provides Singleton]
    fun getStorage(): FirebaseStorage = FirebaseStorage.getInstance()
}