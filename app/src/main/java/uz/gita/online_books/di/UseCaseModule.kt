package uz.gita.online_books.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uz.gita.online_books.domain.usecase.*
import uz.gita.online_books.domain.usecase.impl.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface UseCaseModule {

    @Binds
    @Singleton
    fun getBookUseCase(impl: BookUseCaseImpl): BookUseCase

    @Binds
    @Singleton
    fun getFavUseCase(impl: FavUseCaseImpl): FavUseCase

    @Binds
    @Singleton
    fun getReadUseCase(impl: ReadUseCaseImpl): ReadUseCase

    @Binds
    @Singleton
    fun getUploadUseCase(impl: UploadUseCaseImpl): UploadUseCase

    @Binds
    @Singleton
    fun getAdminUseCase(impl: AdminUseCaseImpl): AdminUseCase
}
