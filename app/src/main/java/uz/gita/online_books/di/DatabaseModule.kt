package uz.gita.online_books.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uz.gita.online_books.data.source.room.BooksDatabase
import uz.gita.online_books.data.source.room.dao.BookDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule{

    @[Provides Singleton]
    fun database( @ApplicationContext context: Context): BooksDatabase =
        Room.databaseBuilder(context, BooksDatabase::class.java, "books.db")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries().build()

    @[Provides Singleton]
    fun dao(booksDatabase: BooksDatabase): BookDao = booksDatabase.dao()

}
