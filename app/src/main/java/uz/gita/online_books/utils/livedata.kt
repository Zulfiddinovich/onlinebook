package uz.gita.online_books.utils

import androidx.lifecycle.MutableLiveData
import uz.gita.online_books.data.model.common.BookAddRequestData


val loadCompleteLiveData = MutableLiveData<BookAddRequestData>()

val loadStartedLiveData = MutableLiveData<BookAddRequestData>()